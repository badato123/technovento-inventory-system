package com.Technovento_Nunez.tms.Repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.Technovento_Nunez.tms.Entity.PaymentEntity;

public interface PaymentRepository extends JpaRepository<PaymentEntity,Integer>{
	PaymentEntity findByPaymentid(int paymentid);
}
