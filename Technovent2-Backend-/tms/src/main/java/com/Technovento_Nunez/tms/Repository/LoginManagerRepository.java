package com.Technovento_Nunez.tms.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Technovento_Nunez.tms.Entity.LoginManagerEntity;

public interface LoginManagerRepository  extends JpaRepository<LoginManagerEntity, Integer>{
	LoginManagerEntity findByLoginid(int loginid);

}
