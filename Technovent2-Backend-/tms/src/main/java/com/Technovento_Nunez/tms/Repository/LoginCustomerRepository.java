package com.Technovento_Nunez.tms.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Technovento_Nunez.tms.Entity.LoginCustomerEntity;

public interface LoginCustomerRepository extends JpaRepository<LoginCustomerEntity,Integer>{
	LoginCustomerEntity findByLoginid(int loginid);
}
