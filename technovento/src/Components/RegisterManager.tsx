import React, { useRef } from "react";
import './Register.css';
import {useNavigate } from "react-router-dom";
import axios from "axios";

function ManagerRegistration() {

    const userRef = useRef<HTMLInputElement>(null);
    const passRef = useRef<HTMLInputElement>(null);
    const firstRef = useRef<HTMLInputElement>(null);
    const lastRef = useRef<HTMLInputElement>(null);
    const ageRef = useRef<HTMLInputElement>(null);
    const salaryRef = useRef<HTMLInputElement>(null);
    const repassRef = useRef<HTMLInputElement>(null);


    const navigate = useNavigate();

 
  
    const goToMain = () => {
      navigate('/');
    };

    const registerM = async () => {
        if(passRef.current?.value === "" && userRef.current?.value ==="" && firstRef.current?.value==="" && lastRef.current?.value===""
            && salaryRef.current?.value ==="" && ageRef.current?.value ===""){
                alert("FILL IN ALL INPUTS !!!")
            }
            else if(passRef.current?.value === repassRef.current?.value && passRef.current?.value !== ""){
                axios.post('http://localhost:8080/manager/postManager',
                {
                    username: userRef.current?.value,
                    password: passRef.current?.value,
                    firstname: firstRef.current?.value,
                    lastname: lastRef.current?.value,
                    salary: parseFloat(salaryRef.current?.value+""),
                    age: parseInt(ageRef.current?.value+"")
                }).then(res => {
                    console.log(res.data)
                    goToMain();
                    alert("Customer Sucessfully Registered");
                }).catch(err => console.log(err))
            }
            else if(passRef.current?.value !== repassRef.current?.value){
                alert("Passwords Do not Match")
            }
    }
  


    return (
        <div className="manager">
            <img src={'/images/arrow.png'} alt="arrow" width={20} height={20} style={{ float: "left", paddingLeft: '30px', paddingTop: '30px' }} onClick={goToMain} />
            <text style={{ float: "left", paddingLeft: '30px', paddingTop: '30px', color: "white" }} onClick={goToMain}>BACK </text>
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            <div className="CusHeader">

            </div>
            <br /><br /><br /><br /><br />
            <div className="inputs">
                <input className="usernamee" placeholder="Username" style={{ color: "black" }} ref={userRef}/>
                <br />
                <input className="firstname" placeholder="First Name" style={{ color: "black" }} ref={firstRef} />
                <br />
                <input className="lastname" placeholder="Last Name" style={{ color: "black" }} ref={lastRef}/>
                <br />
                <input className="age" placeholder="Age" style={{ color: "black" }} ref={ageRef}/>
                <br />
                <input className="salary" placeholder="Salary" style={{ color: "black" }} ref={salaryRef}/>
                <br />
                <input className="passwordd" placeholder="Password" type="password" style={{ color: "black" }} ref={passRef}/>
                <br />
                <input className="reenter" placeholder="Re-enter Password" type="password" style={{ color: "black" }} ref={repassRef} />
            </div>
            <br />
            <div>
                <button className="registerr" onClick={registerM}>REGISTER</button>
                <br /><br /><br /><br /><br /><br />
            </div>
        </div>
    );
}

export default ManagerRegistration;