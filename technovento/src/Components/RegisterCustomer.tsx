import React, { useRef } from "react";
import './Register.css';
import { useNavigate } from 'react-router-dom';
import axios from "axios";


function CustomerRegistration() {

    const userRef = useRef<HTMLInputElement>(null);
    const passRef = useRef<HTMLInputElement>(null);
    const firstRef = useRef<HTMLInputElement>(null);
    const lastRef = useRef<HTMLInputElement>(null);
    const ageRef = useRef<HTMLInputElement>(null);
    const addrRef = useRef<HTMLInputElement>(null);
    const repassRef = useRef<HTMLInputElement>(null);


    const navigate = useNavigate();

    const goToCreate = () => {
    navigate('/Create');
     };

    const goToMain = () => {
    navigate('/');
    };

    const registerC = async () => {
        if(passRef.current?.value === "" && userRef.current?.value ==="" && firstRef.current?.value==="" && lastRef.current?.value===""
            && addrRef.current?.value ==="" && ageRef.current?.value ===""){
                alert("FILL IN ALL INPUTS !!!")
            }
            else if(passRef.current?.value === repassRef.current?.value && passRef.current?.value !== ""){
                axios.post('http://localhost:8080/customer/postCustomer',
                {
                    username: userRef.current?.value,
                    password: passRef.current?.value,
                    firstname: firstRef.current?.value,
                    lastname: lastRef.current?.value,
                    address: addrRef.current?.value,
                    age: parseInt(ageRef.current?.value+"")
                }).then(res => {
                    console.log(res.data)
                    goToMain();
                    alert("Customer Sucessfully Registered");
                }).catch(err => console.log(err))
            }
            else if(passRef.current?.value !== repassRef.current?.value){
                alert("Passwords Do not Match")
            }
    }




    return (
        <div className="customer">
            <img src={'/Images/arrow.png'} alt="arrow" width={20} height={20} style={{ float: "left", paddingLeft: '30px', paddingTop: '30px' }} onClick={goToMain} />
            <text style={{ float: "left", paddingLeft: '30px', paddingTop: '30px', color: "white" }} onClick={goToCreate}>BACK </text>
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            <div className="CusHeader">

            </div>
            <br /><br /><br /><br /><br />
            <div className="inputs">
                <input className="usernamee" placeholder="Username" style={{ color: "black" }} ref={userRef} />
                <br />
                <input className="firstname" placeholder="First Name" style={{ color: "black" }} ref = {firstRef}/>
                <br />
                <input className="lastname" placeholder="Last Name" style={{ color: "black" }} ref ={lastRef}/>
                <br />
                <input className="age" placeholder="Age" style={{ color: "black" }} ref ={ageRef}/>
                <br />
                <input className="address" placeholder="Address" style={{ color: "black" }} ref={addrRef} />
                <br />
                <input className="passwordd" placeholder="Password" type="password" style={{ color: "black" }} ref={passRef}/>
                <br />
                <input className="reenter" placeholder="Re-enter Password" type="password" style={{ color: "black" }} ref={repassRef}/>
            </div>
            <br />
            <div>
                <button className="registerr" onClick={registerC}>REGISTER</button>
                <br /><br /><br /><br /><br /><br />
            </div>
        </div>
    );
}
export default CustomerRegistration;