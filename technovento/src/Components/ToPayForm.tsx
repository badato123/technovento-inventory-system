import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import { Card, TextField } from '@mui/material';
import { useRef, useState } from 'react';
import axios from 'axios';


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

export interface DialogTitleProps {
    id: string;
    children?: React.ReactNode;
    onClose: () => void;
}

export interface itemDetails{
    id:number,
    onClick: () => Promise<void>;
}


function BootstrapDialogTitle(props: DialogTitleProps) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

export default function UpdateItem(props: itemDetails) {

    const cashRef = useRef<HTMLTextAreaElement>();
    const methodRef = useRef<HTMLTextAreaElement>();
    const [open, setOpen] = React.useState(false);

    const [manager, SetManager] = useState({
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        salary: 0,

});

    const [customer, SetCustomer] = useState({
        customerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        address: "",
    });

    const [inventory, setInventory] = useState([{
        itemid: 100,
        itemdescription: "",
        itemname: "",
        itemprice:0,
        quantity: 100,
        manager:manager

}]);

    const [order, setOrder] = useState([{
        orderid: 100,
        total: "",
        quantity: 0,
        inventory:inventory,
        remainingpayment: 0,
        status: false,
        customer:customer,
        itemname: "",

}]);

    const getManager = async () => {
        axios.get('http://localhost:8080/manager/getBymanagerid', {
            params: {
                id: 1
            }
        }).then(res => {
            console.log(SetManager(res.data))
        }).catch(err => console.log(err))
    }

    const getCustomer = async () => {
        axios.get('http://localhost:8080/manager/getBymanagerid', {
            params: {
                username: "JaneDoe"
            }
        }).then(res => {
            console.log(SetManager(res.data))
        }).catch(err => console.log(err))
    }

    const getOrder = async (id:number) => {
        axios.get('http://localhost:8080/order/getByOrderid?orderid='+id).then(res => {
          setOrder(res.data)
          console.log(order)
        }).catch(err => console.log(err))
    }
    

    const postPayment = async (id:number) => {
        getOrder(id);
        console.log(order); console.log(methodRef.current?.value);
                console.log(cashRef.current?.value);
        axios.post('http://localhost:8080/payment/postPayment',
            {
                cash: parseFloat(cashRef.current?.value + ""),
                paymentmethod: methodRef.current?.value,
                order:order[0]
            }).then(res => {
                console.log(res.data)
                props.onClick();
                alert("Payment has been Added Successfully");
                handleClose();
            }).catch(err =>console.log(err))
    }

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="contained" sx={{ backgroundColor: '#285430', padding: "1rem",minWidth: 130}} onClick={handleClickOpen}>
                Add Payment
            </Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    ADD PAYMENT
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <div className="UpdateItem" style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',

                    }}>

                        <Card sx={{ height: 600, minWidth: '590px', backgroundColor: "yellowgreen", borderRadius: '25px', alignItems: 'center' }} style={{ marginTop: '20px' }}>
                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px', maxWidth: '700px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Cash</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={cashRef}></TextField>
                            </div>

                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Payment Method</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={methodRef}></TextField>
                            </div>
                        </Card>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button  variant="contained" sx={{ backgroundColor: 'Green', minWidth: '200px', minHeight: '50px', marginRight:23}} autoFocus onClick={() =>postPayment(props.id)}>
                        CONFIRM
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}