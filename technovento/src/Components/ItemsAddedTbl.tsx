import { AlignHorizontalCenter, Search } from "@mui/icons-material";
import { Button, Card, CardMedia, Checkbox, InputAdornment, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import TheCard, { CardDetails } from "./CardItem";
import SearchIcon from '@mui/icons-material/Search';
import axios from "axios";
import { render } from "@testing-library/react";
import UpdateItem from "./UpdateItem";

declare var test: number;

export default function DenseTable() {


    const searchRef = useRef<HTMLTextAreaElement>(null);

    const [show, setShow] = useState(false);
    const [manager, SetManager] = useState([{
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        salary: 0,
    }]);

    const [item, setItem] = useState([{
        itemid: 100,
        itemname: "No Data",
        quantity: 0,
        itemprice: 0,
        itemdescription: "No Data",
        picturelink:"",
        manager: manager
    }]);



    const getInventory = async () => {

        axios.get('http://localhost:8080/inventory/displayAllItems').then(res => {
            setItem(res.data)
            setShow(true);
            console.log(res.data)
        }).catch(err => console.log(err))

    }

    const searchInventory = async (item: string) => {

        axios.get('http://localhost:8080/inventory/getByItemName', {
            params: {
                itemname: item
            }
        }).then(res => {
            setItem(res.data)
            setShow(true);
            console.log(res.data)
        }).catch(err => console.log(err))

    }


    const deleteItem = async (id: number) => {
        axios.delete('http://localhost:8080/inventory/deleteItem/' + id).then(res => {
            getInventory()
        })

    }




    return (
        <div className="viewTable">
            <TextField
                id="outlined-basic"
                variant="outlined"
                label="Search Items"
                inputRef={searchRef}
                sx={{
                    bgcolor: "#A4BE7B", marginLeft: "4rem", borderRadius: '30px', width: 650, marginTop: 5
                }}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <SearchIcon />
                        </InputAdornment>
                    ),
                }}
            />
            <Button variant="contained" sx={{ backgroundColor: 'green', padding: '1rem', marginTop: 5, marginLeft: 3 }}
                onClick={
                    () => {
                        if (searchRef.current?.value === "") {
                            getInventory();
                        } else {
                            searchInventory(searchRef.current?.value + "")
                        }
                    }
                }>SearchItem</Button>

            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <TableContainer component={Paper} style={{ alignItems: "center", maxWidth: 1600 }} sx={{ marginTop: 10 }}  >
                    <Table style={{ width: 1600, borderCollapse: 'separate', borderSpacing: '0px 5px', borderRadius: '10px' }} size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ fontSize: 16 }}>ITEM</TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>PRICE</TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>QUANTITY</TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>DESCRIPTION</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {item.map((row, i) => (
                                <TableRow
                                    key={row.itemid}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 }, backgroundColor: "#C3DBBC" }}
                                    style={{ height: 180 }}
                                >
                                    <TableCell component="th" scope="row" sx={{ fontSize: 18 }}> {row.itemname}</TableCell>
                                    <TableCell align="center">
                                        <Card sx={{ width: 110, height: 120, backgroundColor: '#A4BE7B' }}>
                                            
                                            <CardMedia component="img" height={120} width={110} image={row.picturelink} />
                                            

                                          
                                        </Card>
                                    </TableCell>
                                    <TableCell align="center" sx={{ fontSize: 18 }}>{row.itemprice}</TableCell>
                                    <TableCell align="center" sx={{ fontSize: 18 }}>{row.quantity}</TableCell>
                                    <TableCell align="center" sx={{ fontSize: 18 }}>{row.itemdescription}</TableCell>
                                    <TableCell align="center" sx={{ fontSize: 18 }}>
                                        <UpdateItem id={row.itemid} itemname={row.itemname} itemprice={row.itemprice} itemdescription={row.itemdescription} itemquantity={row.quantity } picturelink={row.picturelink} onClick={() => getInventory()}></UpdateItem>
                                        <Button variant="contained" sx={{ backgroundColor: 'Red', padding: '1rem', marginTop: 2 }} onClick={() => { deleteItem(row.itemid) }}>Delete Item</Button>

                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            <div>
                {
                    (() => {
                        if (show !== true) {
                            return <Button variant="contained" sx={{ backgroundColor: '#285430', padding: "1rem", marginTop: 18 }} onClick={getInventory}>Show Items</Button>
                        }
                    })()
                }
            </div>
        </div>
    );
}