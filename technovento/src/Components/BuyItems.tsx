import { Box, Button, Card, CardMedia, Checkbox, Icon, InputAdornment, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import { buyitemDetails } from './BuyItemCard';
import TheCardBuyItem from './BuyItemCard';
import { createRef, useEffect, useReducer, useRef, useState } from 'react';
import Icons from './Icon';
import SearchIcon from '@mui/icons-material/Search';
import axios from 'axios';
import { itemDetails } from './UpdateItem';
import React from 'react';
import BuyRef from './BuyRef';




export default function DenseTable() {

    const searchRef = useRef<HTMLTextAreaElement>(null);
    const quantityRef = React.useRef([]);
    const [quantity, setQuantity] = React.useState({});
    const [ignored, forceUpdate] = useReducer(x => x + 1 , 0);
     

    const [show, setShow] = useState(false);
    
    
    const [ctrArr, serCtrs] = useState<number[]>([])
    let arr = [...ctrArr]
    const [cards, setCards] = useState([
        { title:"",picturelink:"", remainingpayment:0, description:"", price:0, Availablequantity:0, quantity:0  }
      ])


    const [customer, setCustomer] = useState([{
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        address: "",
    }]);

    const [logcustomer, setLogcustomer] = useState([{
        loginid: 100,
        username: "",
        password: "",
        customer: customer

    }]);

    const [manager, SetManager] = useState([{
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        salary: 0,
    }]);

    const [item, setItem] = useState([{
        itemid: 100,
        itemname: "No Data",
        quantity: 0,
        itemprice: 0,
        counter: 0,
        itemdescription: "No Data",
        picturelink:"",
        manager: manager
    }]);

    const [item2, setItem2] = useState([{
        itemid: 100,
        itemname: "No Data",
        quantity: 0,
        itemprice: 0,
        counter: 0,
        itemdescription: "No Data",
        picturelink:"",
        manager: manager
    }]);
    const getLoggedCustomer = async () => {
        axios.get('http://localhost:8080/logincustomer/displayloggedCustomer').then(res => {
            setLogcustomer(res.data)
            console.log(logcustomer)
        }).catch(err => console.log(err))
        forceUpdate();
    }

    const searchInventory2 = async (item: string) => {

        axios.get('http://localhost:8080/inventory/getByItemName', {
            params: {
                itemname: item
            }
        }).then(res => {
            setItem2(res.data)
            setShow(true);
            console.log("item: ",item2)
        }).catch(err => console.log(err))
        forceUpdate();

    }

    const getCustomer = async () => {
        getLoggedCustomer()
        axios.get('http://localhost:8080/customer/getByUserName', {
            params: {
                username: logcustomer[0].username
            }
        }).then(res => {
            setCustomer(res.data)
            console.log("customer: ",customer)
        }).catch(err => console.log(err))
        forceUpdate();
    }
    

     const buyItem = async (itemname: string, quantityy: number) => {
        getCustomer();
        buyItem2(itemname,quantityy);
        forceUpdate();
    }
    const buyItem2 = async (itemname: string, quantityy: number) => { 
        searchInventory2(itemname)
        axios.post('http://localhost:8080/order/postOrder',
            {
                quantity: quantityy,
                status: false,
                inventory: item2[0],
                customer: logcustomer[0].customer
            }).then(res => {
                console.log(res.data)
                getInventory()
                alert("Item Sucessfully Bought");
            }).catch(err => {console.log(err)})
            forceUpdate();
              
                
    }

    
        const getInventory  = async () => {
        getLoggedCustomer()
        axios.get('http://localhost:8080/inventory/displayAllItems').then(res => {
            setItem(res.data)
            setShow(true);
            console.log(res.data)
        }).catch(err => console.log(err))
        forceUpdate();

    }
    useEffect(() => {getCustomer()}, []);
    useEffect(() => {getInventory()}, []);

    const searchInventory = async (item2: string) => {
        searchInventory2(item2)
        axios.get('http://localhost:8080/inventory/getByItemName', {
            params: {
                itemname: item2
            }
        }).then(res => {
            setItem(res.data)
            console.log(item)
            setShow(true);
           
        }).catch(err => console.log(err))
        forceUpdate();
    }


    return (
        <div className="viewTable">
            <TextField
                id="outlined-basic"
                variant="outlined"
                label="Search Items"
                inputRef={searchRef}
                sx={{
                    bgcolor: "#A4BE7B", marginLeft: "4rem", borderRadius: '30px', width: 650, marginTop: 5
                }}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <SearchIcon />
                        </InputAdornment>
                    ),
                }}

            />
            <Button variant="contained" sx={{ backgroundColor: 'green', padding: '1rem', marginTop: 5, marginLeft: 3 }}
                onClick={
                    () => {
                        if (searchRef.current?.value === "") {
                            getInventory();
                        } else {
                            searchInventory(searchRef.current?.value + "")
                        }
                    }
                }>Search Item</Button>


            <TableContainer component={Paper} style={{ alignItems: "center", maxWidth: 1600 }} sx={{ marginTop: 1, marginLeft: 4 }} >
                <Table style={{ width: 1600, borderCollapse: 'separate', borderSpacing: '0px 5px', borderRadius: '10px' }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ITEM</TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}>DESCRIPTION</TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}>PRICE</TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}>AVAILABLEQUANTITY</TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
                            <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {item.map((row, i) => (
                           <BuyRef key={i} itemid={row.itemid} itemname={row.itemname} quantity={row.quantity} itemprice={row.itemprice} itemdescription={row.itemdescription} picturelink={row.picturelink} buyItem = {buyItem}/>
                        ))}

                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}