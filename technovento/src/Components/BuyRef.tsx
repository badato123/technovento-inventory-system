import { Button, Card, CardMedia, TableCell, TableRow, TextField } from "@mui/material";
import React from "react";
import { useRef } from "react";


interface Buyy{
    itemid: number;
    itemname: string;
    quantity: number;
    itemprice: number;
    itemdescription: string;
    picturelink: string;
    buyItem:(itemname: string, quantity: number) => void;
}

export default function Buy(row:Buyy) {

    
    
    const quantityRef = React.useRef<HTMLTextAreaElement>(null);
    const fetchquantity = (): number => {
            
            return parseInt(quantityRef.current?.value+"");
           
    }
    return (
        
               <TableRow
                                key={row.itemid}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 }, backgroundColor: "#C3DBBC", height: "50px", padding: '10px' }}
                                style={{ height: 180 }}
                            >
                                <TableCell component="th" scope="row" sx={{ fontSize: 16 }}>
                                    {row.itemname}
                                </TableCell>
                                <TableCell>
                                    <Card sx={{ width: 110, height: 120, backgroundColor: '#A4BE7B' }}>
                                        <CardMedia component="img" height={120} width={110} image={row.picturelink} />
                                    </Card></TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>{row.itemdescription}</TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>{row.itemprice}</TableCell>
                                <TableCell align="center" sx={{ fontSize: 16 }}>{row.quantity}</TableCell>
                                <TableCell align="center"><TextField placeholder='Enter Quantity' inputRef = {quantityRef} type="text"/></TableCell>
                                
                                <TableCell align="center"><div style={{ display: "flex", justifyContent: "center" }}>
                                    <Button variant="contained" color="success" sx={{ height: 50, marginTop: 2, marginRight: 6 }} onClick={() => { row.buyItem(row.itemname,fetchquantity())}}>Buy Item</Button></div>
                                </TableCell>
                            </TableRow>
        
    )

}