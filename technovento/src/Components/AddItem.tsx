import { Opacity } from "@mui/icons-material";
import { Outlet, Link } from "react-router-dom";
import { Box, Button, Card, TextField } from "@mui/material";
import './AddItem.css';
import { useRef, useState } from "react";
import axios from "axios";



export default function AdditemForm() {

    const nameRef = useRef<HTMLTextAreaElement>(null);
    const quantityRef = useRef<HTMLTextAreaElement>(null);
    const priceRef = useRef<HTMLTextAreaElement>(null);
    const descriptionRef = useRef<HTMLTextAreaElement>(null);
    const pictureRef = useRef<HTMLTextAreaElement>(null);


    const [manager, SetManager] = useState({
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        salary: 0,

    });

    const [logmanager, setLogmanager] = useState([{
        loginid: 100,
        username: "",
        password: "",
        manager: manager,

    }]);


    


    const getManager = async () => {
        axios.get('http://localhost:8080/manager/getBymanagerid', {
            params: {
                id: logmanager[0].manager.managerid
            }
        }).then(res => {
            console.log(SetManager(res.data))
        }).catch(err => console.log(err))
    }

    const getLoggedManager = async () => {
        axios.get('http://localhost:8080/loginmanager/displayLoginManager').then(res => {
            setLogmanager(res.data)
        }).catch(err => console.log(err))
    }



    const postInventory = async () => {
        getLoggedManager();
        getManager();
        axios.post('http://localhost:8080/inventory/postItem',
            {
                itemname: nameRef.current?.value,
                quantity: parseInt(quantityRef.current?.value + ""),
                itemprice: parseFloat(priceRef.current?.value + ""),
                itemdescription: descriptionRef.current?.value,
                picturelink: pictureRef.current?.value,
                manager: manager
            }).then(res => {
                console.log(res.data)
                alert("A new Item has been succesfully added");
            }).catch(err => console.log(err))
    }



    return (
        <div className="ItemsAdded" style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',

        }}>

            <Card sx={{ height: 850, minWidth: '700px', backgroundColor: '#FFFFFF', borderRadius: '40px', alignItems: 'center' }} style={{ marginTop: '80px' }}>
                <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px', maxWidth: '700px' }}>
                    <div style={{ padding: '15px', fontSize: '20px' }}>Item Name</div>
                    <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '600px' }} inputRef={nameRef}></TextField>
                </div>

                <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                    <div style={{ padding: '15px', fontSize: '20px' }}>Item Price</div>
                    <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '600px' }} inputRef={priceRef}></TextField>
                </div>

                <div style={{ backgroundColor: "green", margin: "25px", paddingBottom: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                    <div style={{ padding: '15px', fontSize: '20px' }}>Item Quantity</div>
                    <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '600px' }} inputRef={quantityRef}></TextField>
                </div>

                <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                    <div style={{ padding: '15px', fontSize: '20px' }}>Item Description</div>
                    <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '600px' }} inputRef={descriptionRef}></TextField>
                </div>
                
                <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                    <div style={{ padding: '15px', fontSize: '20px' }}>Picture Link</div>
                    <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '600px' }} inputRef={pictureRef}></TextField>
                </div>
                <Button variant="contained" sx={{ backgroundColor: 'Green', minWidth: '200px', minHeight: '50px' }} onClick={postInventory}>Add Item</Button>
            </Card>
        </div>

    );

}