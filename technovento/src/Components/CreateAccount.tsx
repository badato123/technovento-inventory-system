import React from "react";
import './Create.css';
import { useNavigate } from 'react-router-dom';

function Create() {
  const navigate = useNavigate();

  const goToMain = () => {
    navigate('/');
  };

  const goToCusReg = () => {
    navigate('/LoginCustomer');
  };

  const goToManReg = () => {
    navigate('/LoginManager');
  };

    return (
      <div className="Create" style={{color: "white"}}>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
          <div className="SignHeader" style={{marginRight: '90%'}}>
          </div>
          <br/>
          <br/>
          <div className="or"><br/><br/>
            <button className="signupman" style={{marginLeft: '520px'}} onClick={goToManReg}>Login As Manager</button>
            <div className='or' style={{marginLeft: '520px', color: "black"}}>_________________ OR _________________</div><br/>
            <button className="signupcus" style={{marginLeft: '520px'}} onClick={goToCusReg}>Login As Customer</button>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
          </div>
      </div>
    );
  }
  
  
  
  export default Create;