import { Box, Button, Card, CardMedia, Checkbox, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';import { useEffect, useState } from 'react';
import axios from 'axios';
import './Tables.css';



  interface APIResponseType {
    image: string;

  }
export default function DenseTable() {

const [manager, SetManager] = useState([{
  managerid: 100,
  username: "",
  password: "",
  firstname: "",
  lastname: "",
  age: 0,
  salary: 0,
}]);
const [customer, SetCustomer] = useState([{
  customerid: 100,
  username: "",
  password: "",
  firstname: "",
  lastname: "",
  age: 0,
  address: "",
}]);
const [inventory, SetInventory] = useState([{
  itemid: 100,
  itemname: "",
  itemdescription: "",
  quantity: "",
  itemprice: "",
  manager:manager,
}]);
const [orders,setOrders]= useState([{
  orderid: 1,
  total: "",
  quantity: "",
  inventory:inventory,
  remainingpayment: "",
  status: 0,
  customer:customer,
  itemname: "",
}])
const [show, setShow] = useState(false);
useEffect(()=>{
  loadOrder();
},[])

const loadOrder = async()=>{
  const url='http://localhost:8080/order/getAllOrder';
  axios.get(url)
  .then(res => {
      console.log(res.data)
      setOrders(res.data)
      setShow(true);
  })
  .catch(err =>{
    console.log(err,"nodata")
  });
console.log("channel");
}

const deleteOrders = async (orderid: number) =>{
    await axios.delete("http://localhost:8080/order/deleteOrder/"+orderid)  
    .then(() => alert('Delete successful'));
    loadOrder()
  .catch(()=>alert("Delete failed"))
}
  return (
    <div className="viewTable">
      <TableContainer component={Paper} style={{ alignItems: "center", maxWidth: 1600 }} sx={{ marginTop: 10, marginLeft: 2 }} >
        <Table style={{ width: 1600, borderCollapse: 'separate', borderSpacing: '0px 5px', borderRadius: '10px' }} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>ITEM</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>QUANTITY</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>REMAINING PAYMENT</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>TOTAL</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orders.map((row, i) => (
              <TableRow
                key={row.orderid}
                sx={{ '&:last-child td, &:last-child th': { border: 0 }, backgroundColor: "#C3DBBC", height: "50px", padding: '10px' }}
                style={{ height: 180 }}>
                <TableCell component="th" scope="row" sx={{ fontSize: 16 }}> {row.itemname}</TableCell>
                <TableCell> <Card sx={{ width: 110, height: 120, backgroundColor: '#A4BE7B' }}> <CardMedia component="img" height={120} width={110} image='/images/app14-1.jpeg' /> </Card></TableCell>
                <TableCell align="center" sx={{ fontSize: 16 }}>{row.quantity}</TableCell>
                <TableCell align="center" sx={{ fontSize: 16 }}>{row.remainingpayment}</TableCell>
                <TableCell align="center" sx={{fontSize: 18}}>{row.total}</TableCell>
                <TableCell align="center" sx={{fontSize: 18}}> 
                    <Button>
                      <div>{(()=>{
                          if (row.total == '0'|| row.total == row.remainingpayment ) {
                              return <Button onClick={()=>deleteOrders(row.orderid)} variant="contained" sx={{ backgroundColor: "darkolivegreen", padding: '1rem'}}>Cancel Order</Button>}
                              })()
                           }
                        </div>
                      </Button>
                  </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    
    </div>
  );
}