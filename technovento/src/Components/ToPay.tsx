import { Box, Button, Card, CardMedia, Checkbox, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import TheCardOrder from './BuyItemCard';
import * as React from 'react';
import TheCardPayment from './ToPayItems';
import TheCard, { PaymentDetails } from './ToPayItems';
import SearchIcon from '@mui/icons-material/Search';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
import ToPayForm from './ToPayForm';

const Cards: PaymentDetails[] = [
  { title: 'Iphone 14', quantity: "40", Remainingpayment: 'PHP 4000', image: '/images/app14-1.jpeg' },
  { title: 'MacBook Air', quantity: "40", Remainingpayment: 'PHP 4000', image: '/images/macbook.jpeg' },
  { title: 'Iphone 13', quantity: "23", Remainingpayment: 'PHP 4000', image: '/images/app13.jpeg' }
]


export default function DenseTable() {

const [manager, SetManager] = useState([{
    managerid: 100,
    username: "",
    password: "",
    firstname: "",
    lastname: "",
    age: 0,
    salary: 0,

}]);

const [customer, SetCustomer] = useState([{
    customerid: 100,
    username: "",
    password: "",
    firstname: "",
    lastname: "",
    age: 0,
    address: "",
}]);

const [inventory, SetInventory] = useState([{
  itemid: 100,
  itemname: "",
  itemdescription: "",
  quantity: "",
  itemprice: "",
  manager:manager,
}]);

const [order, SetOrder] = useState([{
  orderid: 100,
  total: "",
  quantity: "",
  inventory:inventory,
  remainingpayment: "",
  status: 0,
  customer:customer,
  itemname: "",

}]);
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

  const getOrder = async () => {

    axios.get('http://localhost:8080/order/getAllOrder').then(res => {
        SetOrder(res.data)
        setShow(true);
        console.log(res.data)
    }).catch(err => console.log(err))

}

  return (
    <div className="viewTable">
      <TextField
        id="outlined-basic"
        variant="outlined"
        label="Search Order"
        sx={{
          bgcolor: "#A4BE7B", marginLeft: "4rem", borderRadius: '30px', width: 650, marginTop: 5
        }}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}

      />
      <TableContainer component={Paper} style={{ alignItems: "center", maxWidth: 1600 }} sx={{ marginTop: 10, marginLeft: 2 }} >
        <Table style={{ width: 1600, borderCollapse: 'separate', borderSpacing: '0px 5px', borderRadius: '10px' }} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>ITEM</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>QUANTITY</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>REMAINING PAYMENT</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}>STATUS</TableCell>
              <TableCell align="center" sx={{ fontSize: 16 }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {order.map((row, i) => (
              <TableRow
                key={row.orderid}
                sx={{ '&:last-child td, &:last-child th': { border: 0 }, backgroundColor: "#C3DBBC", height: "50px", padding: '10px' }}
                style={{ height: 180 }}
              >
                <TableCell component="th" scope="row" sx={{ fontSize: 16 }}>
                  {row.itemname}
                </TableCell>
                <TableCell>
                  <Card sx={{ width: 110, height: 120, backgroundColor: '#A4BE7B' }}>
                    <CardMedia component="img" height={120} width={110} image='/images/app14-1.jpeg' />
                  </Card></TableCell>
                <TableCell align="center" sx={{ fontSize: 16 }}>{row.quantity}</TableCell>
                <TableCell align="center" sx={{ fontSize: 16 }}>{row.remainingpayment}</TableCell>
                <TableCell align="center"><Checkbox {...label} defaultChecked color="success" /></TableCell>
                <TableCell align="center"><ToPayForm id={row.orderid} onClick={() =>getOrder()}/></TableCell>
              </TableRow>
            ))}

          </TableBody>
        </Table>
      </TableContainer>
      <div>
                {
                    (() => {
                        if (show !== true) {
                            return <Button variant="contained" sx={{ backgroundColor: '#285430', padding: "1rem", marginTop: 18 }} onClick={getOrder}>Show Items</Button>
                        }
                    })()
                }
            </div>
    </div>
  );
}