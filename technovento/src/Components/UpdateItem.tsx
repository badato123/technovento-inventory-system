import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import { Card, TextField } from '@mui/material';
import { useRef, useState } from 'react';
import axios from 'axios';


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

export interface DialogTitleProps {
    id: string;
    children?: React.ReactNode;
    onClose: () => void;
}

export interface itemDetails{
    id:number,
    itemname:string,
    itemprice:number,
    itemquantity:number,
    itemdescription:string,
    picturelink:string
    onClick: () => Promise<void>;
}


function BootstrapDialogTitle(props: DialogTitleProps) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

export default function UpdateItem(props: itemDetails) {

    const nameRef = useRef<HTMLTextAreaElement>();
    const quantityRef = useRef<HTMLTextAreaElement>();
    const priceRef = useRef<HTMLTextAreaElement>();
    const descriptionRef = useRef<HTMLTextAreaElement>();
    const pictureRef = useRef<HTMLTextAreaElement>();
    const [open, setOpen] = React.useState(false);


    const [manager, SetManager] = useState({
        managerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        salary: 0,

});

    const getManager = async () => {
        axios.get('http://localhost:8080/manager/getBymanagerid', {
            params: {
                id: 1
            }
        }).then(res => {
            console.log(SetManager(res.data))
        }).catch(err => console.log(err))
    }
    

    const putItem = async () => {
        getManager()
        axios.put('http://localhost:8080/inventory/putItem?id='+props.id,
            {
                itemname: nameRef.current?.value,
                quantity: parseInt(quantityRef.current?.value+""),
                itemprice: parseFloat(priceRef.current?.value +""),
                itemdescription: descriptionRef.current?.value,
            }).then(res => {
                console.log(res.data)
                handleClose();
                props.onClick();
                alert("An Item has been succesfully updated");
            }).catch(err => console.log(err))
            handleClose();
        
    }

    const handleClickOpen = () => {
        setOpen(true);
        console.log(nameRef);
    };
    const handleClose = () => {
        setOpen(false);
        console.log(nameRef);
    };

    return (
        <div>
            <Button variant="contained" sx={{ backgroundColor: '#285430', padding: "1rem",minWidth: 130}} onClick={handleClickOpen}>
                EDIT ITEM
            </Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Update Item
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <div className="UpdateItem" style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',

                    }}>

                        <Card sx={{ height: 1000, minWidth: '590px', backgroundColor: "yellowgreen", borderRadius: '25px', alignItems: 'center' }} style={{ marginTop: '20px' }}>
                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px', maxWidth: '700px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Item Name</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={nameRef} defaultValue ={props.itemname}></TextField>
                            </div>

                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Item Price</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={priceRef} defaultValue ={props.itemprice}></TextField>
                            </div>

                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Picture Link</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={pictureRef} defaultValue ={props.picturelink}></TextField>
                            </div>

                            <div style={{ backgroundColor: "green", margin: "25px", paddingBottom: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Item Quantity</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={quantityRef} defaultValue ={props.itemquantity}></TextField>
                            </div>

                            <div style={{ backgroundColor: "green", margin: "25px", paddingRight: '5px', paddingTop: '5px', borderRadius: '20px' }}>
                                <div style={{ padding: '15px', fontSize: '20px' }}>Item Description</div>
                                <TextField id="standard-basic" variant="standard" sx={{ padding: '15px', minWidth: '300px' }} inputRef={descriptionRef} defaultValue ={props.itemdescription}></TextField>
                            </div>
                        </Card>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button  variant="contained" sx={{ backgroundColor: 'Green', minWidth: '200px', minHeight: '50px', marginRight:23}} autoFocus onClick={putItem}>
                        Save changes
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}