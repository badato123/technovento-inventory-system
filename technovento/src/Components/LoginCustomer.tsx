import React, { useRef, useState } from "react";
import './Login.css';
import { useNavigate } from 'react-router-dom';
import axios from "axios";


function Login() {
    const navigate = useNavigate();

    const usernameRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const[checker,setChecker] = useState("Login Successful");


    const goToCreate = () => {
        navigate('/CusReg');
    };

    function goToHome(){
        navigate('/Customer');
    };


    const [customer, setCustomer] = useState({
        customerid: 100,
        username: "",
        password: "",
        firstname: "",
        lastname: "",
        age: 0,
        address:"",

});

    const loginC = async () => {
        axios.post('http://localhost:8080/logincustomer/Customer',
            {
                username: "",
                password: "",
                customer: customer
            },
            {
                params:{
                    username: usernameRef.current?.value,
                    password: passwordRef.current?.value
                }
            }
            
            ).then(res => {
                console.log(res.data)
                if(res.data === checker){
                    goToHome();
                }
                else{
                    alert("Invalid Credentials");
                }

            }).catch(err => console.log(err))
    }






    return (
        <div className="Main"><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            <div className="LoginHeader">

            </div>
            <br />
            <div className="inputs">
                <input className="username" placeholder="Username" style={{ color: "black", marginLeft: '500px' }} ref={usernameRef} />
                <br />
                <input className="password" placeholder="Password" type="password" style={{ color: "black", marginLeft: '500px' }} ref={passwordRef} />
            </div>
            <br />
            <div className="or">
                <button className="login" style={{ marginLeft: '500px' }} onClick={loginC}>LOG IN</button>
                <div className='or' style={{ marginLeft: '500px', color: "black" }}>__________________ OR __________________</div><br />
                <button className="createnew" style={{ marginLeft: '500px' }} onClick={goToCreate} >Create New Account</button>
                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>
    );
}

export default Login;